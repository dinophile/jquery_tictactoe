$(document).on('ready', function(){
	var turn = 0;
	// var winningCombos = [[1,2,3], [4,5,6], [7,8,9], [1,5,9], [1,4,7],[2,5,8],[3,6,9]];

	$('td').on('click', function(){
		if (turn%2===0){
			$(this).html('X').addClass('X');
		}else{
			$(this).html('O').addClass('O');
		}

		function checkWinner() {
			var won = 0;
			if ($('.1').hasClass('X') && $('.2').hasClass('X') && $('.3').hasClass('X')) {
				won = 1
			}else if ($('.1').hasClass('X') && $('.4').hasClass('X') && $('.7').hasClass('X')) {
				won = 1
			}else if ($('.1').hasClass('X') && $('.5').hasClass('X') && $('.9').hasClass('X')) {
				won = 1
			}else if ($('.4').hasClass('X') && $('.5').hasClass('X') && $('.6').hasClass('X')) {
				won = 1
			}else if ($('.7').hasClass('X') && $('.8').hasClass('X') && $('.9').hasClass('X')) {
				won = 1
			}else if ($('.2').hasClass('X') && $('.5').hasClass('X') && $('.8').hasClass('X')) {
				won = 1
			}else if ($('.3').hasClass('X') && $('.6').hasClass('X') && $('.9').hasClass('X')) {
				won = 1
			}else if ($('.3').hasClass('X') && $('.5').hasClass('X') && $('.7').hasClass('X')) {
				 won = 1
			}else if ($('.1').hasClass('O') && $('.4').hasClass('O') && $('.7').hasClass('O')) {
				won = 2
			}else if ($('.1').hasClass('O') && $('.4').hasClass('O') && $('.7').hasClass('O')) {
				won = 2
			}else if ($('.1').hasClass('O') && $('.5').hasClass('O') && $('.9').hasClass('O')) {
				won = 2
			}else if ($('.4').hasClass('O') && $('.5').hasClass('O') && $('.6').hasClass('O')) {
				won = 2
			}else if ($('.7').hasClass('O') && $('.8').hasClass('O') && $('.9').hasClass('O')) {
				 won = 2
			}else if ($('.2').hasClass('O') && $('.5').hasClass('O') && $('.8').hasClass('O')) {
				won = 2
			}else if ($('.3').hasClass('O') && $('.6').hasClass('O') && $('.9').hasClass('O')) {
				won = 2
			}else if ($('.3').hasClass('O') && $('.5').hasClass('O') && $('.7').hasClass('O')) {
				won = 2
			}else if ($('td:empty').length === 0){
				won = 3
			};
			return won;
		};

		function whoHasWon(){
			if (checkWinner() === 1){
				$('.messages').html('The X\'s have won!');
				$('td').off('click');
			}else if (checkWinner() === 2){
				$('.messages').html('The O\'s have won!');
				$('td').off('click');
			}else if (checkWinner() === 3){
				$('.messages').html('It\'s a tie!');
			}
		};

		$(this).off('click');
		turn++
		whoHasWon();

	});

	$('.reset').click(function() {
		document.location.reload(true);
	});


});








	// $('.reset').click(function() {
	// 	messages.html('');
	// 	$('td').each(function() {
	// 		$(this).removeClass('O').removeClass('X');
	// 	});
	// });

//stretch
// detect a draw
// reset button
// add computer player
